CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers
 * API notes

INTRODUCTION
------------
Mapit (http://drupal.org/project/iati) is a Drupal 7 module that
handles IATI data by allowing users to capture data about Activities
and Organisations according to the IATI 1.03 statndard. The module also
provides the ability for user to import IATI Activities and Organisations

For more information on IATI visit http://drupal.org/node/_______

REQUIREMENTS
------------
This module requires the following modules:
 *Corresponding Entity References (https://drupal.org/project/cer)
 *Entity API (https://drupal.org/project/entity)
 *Entity reference (https://drupal.org/project/entityreference)
 *Geofield (https://drupal.org/project/geofield)
 *OpenLayers (https://drupal.org/project/openlayers)
 *Migrate Exras (https://drupal.org/project/migrate_extras)
 *Migrate UI (https://drupal.org/project/migrate)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
*Enable the migrate UI module and navigate to the admin/content/migrate page
and check all the boxes for the IATI codelists, then select Import to import
them.
*A user can then create IATI activities or IATI organisations via the Add IATI Activity
or Add IATI Organisation pages provided by the module.

TROUBLESHOOTING
---------------

MAINTAINERS
-----------
Current maintainers:
 * Dan Colin Mulindwa (dcmul) - https://drupal.org/user/2505784
 * Aggrey Muhebwa (amuhebwa) - https://drupal.org/user/2639277
 * Joanna Kisaakye (Joanna_Kisaakye) - https://drupal.org/user/2834589
 