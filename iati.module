<?php
/**
 * @file
 * The IATI module file.
 */

/**
 * Implements of hook_permission().
 */
function iati_permission() {
  $permissions = array();

  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity.
  $permissions['administer iati'] = array(
    'title' => t('Administer iati'),
    'description' => t('Administer the iati module'),
  );

  return $permissions;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function iati_entity_property_info_alter(&$info) {
  $properties = &$info['iati_budget']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the budget was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'setter callback' => 'entity_property_verbatim_set',
    'description' => t("The date the budget was most recently updated."),
  );
  $properties['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the budget on the site."),
    'getter callback' => 'entity_metadata_field_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'uid',
  );
  $properties['value_amount'] = array(
    'label' => t("Amount"),
    'type' => 'integer',
    'description' => t("The amount of the budget."),
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'value_amount',
  );
  $properties['value_date'] = array(
    'label' => t("Value Date"),
    'type' => 'date',
    'schema field' => 'value_date',
    'description' => t("The value date of the budget."),
  );
  $properties['period_start'] = array(
    'label' => t("Period Start Date"),
    'type' => 'date',
    'schema field' => 'period_start',
    'description' => t("The start date of the budget."),
  );
  $properties['period_start_text'] = array(
    'label' => t("Period Start Text"),
    'type' => 'date',
    'schema field' => 'period_start',
    'description' => t("The start date of the budget as text."),
  );
  $properties['period_end'] = array(
    'label' => t("Period End Date"),
    'type' => 'date',
    'schema field' => 'period_end',
    'description' => t("The end date of the budget."),
  );
  $properties['period_end_text'] = array(
    'label' => t("Period End Text"),
    'type' => 'date',
    'schema field' => 'period_end',
    'description' => t("The end date of the budget as text."),
  );

  $properties = &$info['iati_location']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the location was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t("Date changed"),
    'type' => 'date',
    'schema field' => 'changed',
    'setter callback' => 'entity_property_verbatim_set',
    'description' => t("The date the location was most recently updated."),
  );
  $properties['uid'] = array(
    'label' => t("Author"),
    'type' => 'user',
    'description' => t("The author of the location on the site."),
    'getter callback' => 'entity_metadata_field_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'uid',
  );
  $properties['name'] = array(
    'label' => t("Name"),
    'type' => 'text',
    'schema field' => 'name',
    'setter callback' => 'entity_property_verbatim_set',
    'description' => t("The name of the location."),
  );
  $properties['description'] = array(
    'label' => t("Description"),
    'type' => 'text',
    'schema field' => 'description',
    'setter callback' => 'entity_property_verbatim_set',
    'description' => t("The description of the location."),
  );
}




//function iati_entity_property_info_alter(&$info) {
                                                             //              //      //debug($info);
 /*
  $properties = &$info['iati_budget']['properties'];
  foreach($properties as &$field) {
    //debug($field);
    $field['validation callback'] = 'iati_entity_metadata_field_validate';
    $field['getter callback'] = 'iati_entity_metadata_field_validate';
    $field['setter callback'] = 'iati_entity_metadata_field_validate';
  }
*/
//}


/**
 * Implements of hook_menu().
 */
function iati_menu() {
  //drupal_set_message("am there");
  $items['admin/config/iati/'] = array(
    'title' => 'IATI',
    'type' => MENU_NORMAL_ITEM,
    'weight' => -50,
    //'access callback' => 'user_access',
    'access arguments' => array('administer iati'),
  );

  $items['admin/config/iati/settings'] = array(
    'title' => 'IATI Settting',
    'description' => 'Set Iati Settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('iati_settings'),
    'access arguments' => array('administer iati'),
  );

   $items['admin/content/iati_budget/manage/view/%iati_budget'] = array(
    'title' => 'View Budget',
    'file' => 'includes/iati_budget.admin.inc',
    'page callback' => 'iati_budget_view',
    'page arguments' => array(5),
    //'access callback' => TRUE,
    'access arguments' => array('administer iati'),
  );

  $items['admin/content/iati_location/manage/view/%iati_location'] = array(
    'title' => 'View Location',
    'file' => 'includes/iati_location.admin.inc',
    'page callback' => 'iati_location_view',
    'page arguments' => array(5),
    //'access callback' => TRUE,
    'access arguments' => array('administer iati'),
  ); 
  return $items;
}

function iati_menu_alter (&$items) {  
  if(isset($items['admin/content/iati_location/add'])) {
    unset($items['admin/content/iati_location/add']);    
  }
  if(isset($items['admin/content/iati_budget/add'])) {
  	unset($items['admin/content/iati_budget/add']);
  }
}
/**
 * The IATI settings.
 */
function iati_settings () {
  $form['iati_settings'] = array(
    '#type' => 'markup',
    '#prefix' => '<div>',
    '#markup' => t('There are no iati settings yet.'),
    '#suffix' => '</div>',
  );

  return system_settings_form ($form);
}

/**
 * Implements hook_form_submit().
 */
function iati_settings_submit ($form, $form_state) {
  // All is fine.
}

/**
 * Implements hook_entity_info().
 *
 * We define the Prizes & Winners entity here
 */
function iati_entity_info () {
  $return['iati_budget'] = array(
    'label' => t('IATI Budget'),
    'entity class' => 'IatiBudgetData',
    'controller class' => 'IatiBudgetDataController',
    'base table' => 'iati_budget',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'bid',
      'label' => 'name',
    ),
    'load hook' => 'iati_data_load',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'access callback' => 'iati_access',
    'module' => 'iati',
    'view modes' => array(
      'full' => array(
        'label' => t('Full Budget'),
        'custom settings' => FALSE,
      ),
    ),
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/content/iati_budget',
      'file' => 'includes/iati_budget.admin.inc',
      'controller class' => 'IatiBudgetUIController',
    ),
    'inline entity form' => array(
      'controller' => 'BudgetInlineEntityFormController',
    ),
  );

  $return['iati_location'] = array(
    'label' => t('IATI Location'),
    'entity class' => 'IatiLocationData',
    'controller class' => 'IatiLocationDataController',
    'base table' => 'iati_location',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'lid',
      'label' => 'name',
    ),
    'load hook' => 'iati_data_load',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'access callback' => 'iati_access',
    'module' => 'iati',
    'view modes' => array(
      'full' => array(
        'label' => t('Full Location'),
        'custom settings' => FALSE,
      ),
    ),
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/content/iati_location',
      'file' => 'includes/iati_location.admin.inc',
      'controller class' => 'IatiLocationUIController',
    ),
    'inline entity form' => array(
      'controller' => 'LocationInlineEntityFormController',
    ),
  );
  return $return;
}

/**
 * Determines whether the given user has access to a iati budget.
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 *
 * @param object $account
 *   The user to check for. Leave it to NULL to check for the global user.
 *
 * @return bool
 *   Whether access is allowed or not.
 */
function iati_access ($op, $type = NULL, $account = NULL, $entitytype = NULL) {
  if (user_access('administer iati', $account)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Load budget entity.
 *
 * @param int $entityid
 *   entity id
 *
 * @return mixed
 *   entity object
 */
function iati_budget_load ($entityid) {
  $return = entity_load("iati_budget", array($entityid));
  return reset($return);
}

/**
 * Load location entity.
 *
 * @param int $entityid
 *   unique entity identifier
 *
 * @return mixed
 *   entity object
 */
function iati_location_load($entityid) {
  $return = entity_load("iati_location", array($entityid));
  return reset($return);

}

function iati_check_valid_iso_date($date) {
  // $testdate = strtotime($date);
   // if (date('Y-m-d', strtotime($data)) == $data) {
        //return true;
   // } else {
        //return false;
    //}
}


function iati_field_widget_form_alter(&$element, &$form_state, $context) {
	if(isset($element['#field_name'])){
	 // These changes will rename the buttons on the organisation page.
	$fieldname = $element['#field_name'];
   if ($fieldname == 'field_recipient_country_budget') {
		$element['actions']['ief_add']['#value'] = t('Add Receipient Countries Budget');
	}
	if ($fieldname == 'field_recipient_org_budget') {
		$element['actions']['ief_add']['#value'] = t('Add Receipient Organisation Budget');
	}
	if ($fieldname == 'field_total_budget') {
		$element['actions']['ief_add']['#value'] = t('Add Annual Total Budget');
	}
	//These changes rename the buttons on the activity page.
	if ($fieldname == 'field_iati_activity_budget') {
	 $element['actions']['ief_add']['#value'] = t('Add New Budget');
	}
	if ($fieldname == 'field_iati_location') {
	 $element['actions']['ief_add']['#value'] = t('Add new Location');
	  }
	}

}




function iati_init() {
  $node = arg(0);
  $add = arg(1);
  $activity = arg(2);
  if($node =='node' && $add =='add' && $activity=='iati-activity') {
      libraries_load('openlayers');
  }

}

